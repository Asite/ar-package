


$(window).on("load", function(){


	$("body").attr("camera", spec.camera);
	$("body").attr("tone", spec.tone);

	$.each(spec, function(i,o){

		$("select[name='"+i+"']").val(o);

	})

	// resize();
	hasGetUserMedia();



});



$(window).on("resize", function(){

	resize();

});

var windowWidth = 0;
var windowHeight = 0;


function resize(){

	windowWidth = $(window).outerWidth();
	windowHeight = $(window).outerHeight();

	var c = document.getElementById("canvas");


	if (c.width > windowWidth) {

		translate = c.width - windowWidth;

	}

	var scaleX = windowWidth / c.width * 1.1;
	var scaleY = windowHeight / c.height * 1.1;

	var translate = $("#container").offset().left;


	if (c.width > c.height) {

		$("#container").css("transform", "scale("+scaleX+")");

		if ($("#container").offset().left > 0) {

			// $("#container").css("margin-left", "-"+$("#container").offset().left+"px");


		}

	} else {

		$("#container").css("transform", "scale("+scaleY+")");

	}





	// $("#video, #canvas").attr("width", windowWidth);
	// $("#video, #canvas").attr("height", windowHeight);

	// scaleX = windowWidth / 640;
	// scaleY = windowWidth / 480;

}

function hasGetUserMedia() {
  return !!(navigator.mediaDevices && navigator.mediaDevices.getUserMedia);
}
if (hasGetUserMedia()) {

	setup();

} else {
  alert("getUserMedia() is not supported by your browser");
}

async function setup(){


	const constraints = {
	    audio: false,
	    video: {
	        width: { min: 1024, ideal: 1280, max: 1920 },
	        height: { min: 576, ideal: 720, max: 1080 },
  			facingMode:  spec.camera    
	    }
	};


	navigator.mediaDevices.getUserMedia(constraints).then((stream) => {

		const video = document.getElementById('video');

		console.log(stream);

		video.srcObject = stream;


		video.addEventListener('loadeddata', (event) => {

			// resize();
			capture();

			var c = document.getElementById("canvas");

			c.width = video.videoWidth;
			c.height = video.videoHeight;

			// alert(c.width);
			// alert(windowWidth);

			resize();

			// $("#video, #canvas").attr("width", windowWidth);
			// $("#video, #canvas").attr("height", windowHeight);			

		});



	});


	// // Create a detector.
	// const detector = await poseDetection.createDetector(poseDetection.SupportedModels.MoveNet);

	// // // Pass in a video stream to the model to detect poses.
	// const video = document.getElementById('video');
	// const poses = await detector.estimatePoses(video);

	// console.log(poses[0].keypoints);
	// // Outputs:
	// // [
	// //    {x: 230, y: 220, score: 0.9, name: "nose"},
	// //    {x: 212, y: 190, score: 0.8, name: "left_eye"},
	// //    ...
	// // ]


	// setupCamera();

}

function hexToRgbA(hex, opacity){
    var c;
    if(/^#([A-Fa-f0-9]{3}){1,2}$/.test(hex)){
        c= hex.substring(1).split('');
        if(c.length== 3){
            c= [c[0], c[0], c[1], c[1], c[2], c[2]];
        }
        c= '0x'+c.join('');
        return 'rgba('+[(c>>16)&255, (c>>8)&255, c&255].join(',')+','+opacity+')';
    }
    throw new Error('Bad Hex');
}

function getColor(opacity) {

	if (spec.color == "random") {



	  var letters = '0123456789ABCDEF';
	  var color = '#';
	  for (var i = 0; i < 6; i++) {
	    color += letters[Math.floor(Math.random() * 16)];
	  }

	} else {

		var color = "#ffffff";

	}

	if (typeof opacity == "undefined") {

		opacity = 1;

	}

	return hexToRgbA(color, opacity);



}


async function capture(){


	const detector = await poseDetection.createDetector(poseDetection.SupportedModels.MoveNet);

	const video = document.getElementById('video');
	var c = document.getElementById("canvas");
	var ctx = c.getContext("2d");

	$("#container").addClass("active");

	// var stop = false;
	// var frameCount = 0;
	// var $results = $("#results");
	// var fps, fpsInterval, startTime, now, then, elapsed;


	// // initialize the timer variables and start the animation

	// function startAnimating(fps) {
	//     fpsInterval = 1000 / fps;
	//     then = Date.now();
	//     startTime = then;
	//     animate();
	// }

	if (spec.mode == "") {

		$("#msg").text("Simple tracking.");

	} else if (spec.mode == "emoji") {

		$("#msg").text("Pose to find 3 hidden emojis.");

	} else if (spec.mode == "sketch") {

		$("#msg").text("Your body is a paintbrush. 🙏 to clear.");

	} else if (spec.mode == "circle") {

		$("#msg").text("Create a circle with your two hands.");

	}

	requestAnimationFrame(async function animate(nowMsec){

		requestAnimationFrame( animate );
		const poses = await detector.estimatePoses(video);

		if (spec.mode == "sketch") {



		} else {

			ctx.clearRect(0, 0, c.width, c.height);

		}




		$.each(poses, function(i, o){

			if (spec.mode == "sketch") {

					if (Math.abs(o.keypoints[9].y - o.keypoints[10].y) < c.height * 0.15 && Math.abs(o.keypoints[9].x - o.keypoints[10].x) < c.width * 0.15 && Math.abs(o.keypoints[7].y - o.keypoints[9].y) < c.height * 0.15  && Math.abs(o.keypoints[8].y - o.keypoints[10].y) < c.height * 0.15) {

						ctx.clearRect(0, 0, c.width, c.height);

					}			

			}



			$.each(o.keypoints, function(j, p){

				// ctx.beginPath();
				// ctx.arc(100, 75, 50, 0, 2 * Math.PI);
				// ctx.stroke();
				// ctx.fill();
				// ctx.moveTo(0, 0);
				// ctx.lineTo(200, 100);
				// ctx.stroke();



				if (p.x > video.videoWidth * 0.95 || p.x < video.videoWidth * 0.05 || p.y > video.videoHeight * 0.95 || p.y < video.videoHeight * 0.05) {

					// return false;

				} else {


					var x = p.x * 1;
					var y = p.y * 1;

					// console.log([x, y]);


					if (spec.overlay == "dot") {

						ctx.beginPath();
						ctx.arc(x, y, 5, 0, 2 * Math.PI);
						ctx.strokeStyle = "black";
						ctx.lineWidth = "10";
						ctx.fillStyle = getColor();
						ctx.stroke();		
						ctx.fill();								

					} else if (spec.overlay == "head") {



					} else if (spec.overlay == "none") {


					}



				}

				




			});


			// console.log(o.keypoints);



			// 0: {y: 322.64454528256124, x: 455.44080238150946, score: 0.4144206643104553, name: 'nose'}
			// 1: {y: 219.9876155840768, x: 586.693671877875, score: 0.5011979341506958, name: 'left_eye'}
			// 2: {y: 191.38417905680123, x: 394.2867016442117, score: 0.5283997058868408, name: 'right_eye'}
			// 3: {y: 184.65806528168827, x: 677.7434752813168, score: 0.395546555519104, name: 'left_ear'}
			// 4: {y: 64.58029980728993, x: 281.22979457189643, score: 0.5136762261390686, name: 'right_ear'}
			// 5: {y: 342.90088547750054, x: 693.8683743352698, score: 0.45095425844192505, name: 'left_shoulder'}
			// 6: {y: 238.37916647546484, x: 26.27478942329133, score: 0.41701462864875793, name: 'right_shoulder'}
			// 7: {y: 686.8401582274219, x: 647.8365639461368, score: 0.5618709921836853, name: 'left_elbow'}
			// 8: {y: 684.7343856040135, x: 82.18316002530875, score: 0.33635857701301575, name: 'right_elbow'}
			// 9: {y: 680.8304935838839, x: 781.0370093954634, score: 0.30034181475639343, name: 'left_wrist'}
			// 10: {y: 645.999313208382, x: 241.70632389331158, score: 0.06537561863660812, name: 'right_wrist'}
			// 11: {y: 623.4790739468616, x: 246.16929688340278, score: 0.30991971492767334, name: 'left_hip'}
			// 12: {y: 545.0407139505676, x: 0.6068693599798136, score: 0.18628844618797302, name: 'right_hip'}
			// 13: {y: 564.5913206852575, x: 371.3936698717064, score: 0.07375908643007278, name: 'left_knee'}
			// 14: {y: 675.3933459274376, x: 51.26287996748332, score: 0.3735898733139038, name: 'right_knee'}
			// 15: {y: 712.1136805493344, x: 45.89922111406408, score: 0.08283968269824982, name: 'left_ankle'}
			// 16: {y: 681.4720930931879, x: 47.55701374484488, score: 0.10522971302270889, name: 'right_ankle'}		
			



			if (spec.overlay == "head") {

					ctx.beginPath();
					ctx.arc(o.keypoints[0].x, o.keypoints[0].y, 5, 0, 2 * Math.PI);
					ctx.strokeStyle = "black";
					ctx.lineWidth = "10";
					ctx.fillStyle =  getColor();
					// ctx.fillStyle = "rgba(255, 0, 0, "+p.score+")";
					ctx.stroke();		
					ctx.fill();								

			} else if (spec.overlay == "wrist_left") {

					ctx.beginPath();
					ctx.arc(o.keypoints[9].x, o.keypoints[9].y, 5, 0, 2 * Math.PI);
					ctx.strokeStyle = "black";
					ctx.lineWidth = "10";
					ctx.fillStyle =  getColor();
					ctx.stroke();		
					ctx.fill();			

			} else if (spec.overlay == "wrist_right") {

					ctx.beginPath();
					ctx.arc(o.keypoints[10].x, o.keypoints[10].y, 5, 0, 2 * Math.PI);
					ctx.strokeStyle = "black";
					ctx.lineWidth = "10";
					ctx.fillStyle =  getColor();
					ctx.stroke();		
					ctx.fill();											

			} else if (spec.overlay == "skeleton") {

				ctx.lineWidth = "5";
				ctx.strokeStyle = getColor();


				ctx.beginPath();
				ctx.moveTo(o.keypoints[3].x, o.keypoints[3].y);
				ctx.lineTo(o.keypoints[1].x, o.keypoints[1].y);
				ctx.lineTo(o.keypoints[0].x, o.keypoints[0].y);
				ctx.lineTo(o.keypoints[2].x, o.keypoints[2].y);
				ctx.lineTo(o.keypoints[4].x, o.keypoints[4].y);
				ctx.stroke();


				ctx.beginPath();
				ctx.moveTo(o.keypoints[5].x, o.keypoints[5].y);
				ctx.lineTo(o.keypoints[6].x, o.keypoints[6].y);
				ctx.lineTo(o.keypoints[12].x, o.keypoints[12].y);
				ctx.lineTo(o.keypoints[11].x, o.keypoints[11].y);
				ctx.lineTo(o.keypoints[5].x, o.keypoints[5].y);
				ctx.stroke();

				ctx.beginPath();
				ctx.moveTo(o.keypoints[10].x, o.keypoints[10].y);
				ctx.lineTo(o.keypoints[8].x, o.keypoints[8].y);
				ctx.lineTo(o.keypoints[6].x, o.keypoints[6].y);
				ctx.lineTo(o.keypoints[5].x, o.keypoints[5].y);
				ctx.lineTo(o.keypoints[7].x, o.keypoints[7].y);
				ctx.lineTo(o.keypoints[9].x, o.keypoints[9].y);				
				ctx.stroke();				

				// ctx.beginPath();
				// ctx.moveTo(o.keypoints[11].x, o.keypoints[11].y);
				// ctx.lineTo(o.keypoints[13].x, o.keypoints[13].y);
				// ctx.lineTo(o.keypoints[15].x, o.keypoints[15].y);
				// ctx.stroke();				


				// ctx.beginPath();
				// ctx.moveTo(o.keypoints[12].x, o.keypoints[12].y);
				// ctx.lineTo(o.keypoints[14].x, o.keypoints[14].y);
				// ctx.lineTo(o.keypoints[16].x, o.keypoints[16].y);
				// ctx.stroke();				



			} else if (spec.overlay == "skeleton_full") {


				ctx.lineWidth = "10";
				ctx.strokeStyle = getColor();


				ctx.beginPath();
				ctx.moveTo(o.keypoints[3].x, o.keypoints[3].y);
				ctx.lineTo(o.keypoints[1].x, o.keypoints[1].y);
				ctx.lineTo(o.keypoints[0].x, o.keypoints[0].y);
				ctx.lineTo(o.keypoints[2].x, o.keypoints[2].y);
				ctx.lineTo(o.keypoints[4].x, o.keypoints[4].y);
				ctx.stroke();


				ctx.beginPath();
				ctx.moveTo(o.keypoints[5].x, o.keypoints[5].y);
				ctx.lineTo(o.keypoints[6].x, o.keypoints[6].y);
				ctx.lineTo(o.keypoints[12].x, o.keypoints[12].y);
				ctx.lineTo(o.keypoints[11].x, o.keypoints[11].y);
				ctx.lineTo(o.keypoints[5].x, o.keypoints[5].y);
				ctx.stroke();

				ctx.beginPath();
				ctx.moveTo(o.keypoints[10].x, o.keypoints[10].y);
				ctx.lineTo(o.keypoints[8].x, o.keypoints[8].y);
				ctx.lineTo(o.keypoints[6].x, o.keypoints[6].y);
				ctx.lineTo(o.keypoints[5].x, o.keypoints[5].y);
				ctx.lineTo(o.keypoints[7].x, o.keypoints[7].y);
				ctx.lineTo(o.keypoints[9].x, o.keypoints[9].y);				
				ctx.stroke();				

				ctx.beginPath();
				ctx.moveTo(o.keypoints[11].x, o.keypoints[11].y);
				ctx.lineTo(o.keypoints[13].x, o.keypoints[13].y);
				ctx.lineTo(o.keypoints[15].x, o.keypoints[15].y);
				ctx.stroke();				


				ctx.beginPath();
				ctx.moveTo(o.keypoints[12].x, o.keypoints[12].y);
				ctx.lineTo(o.keypoints[14].x, o.keypoints[14].y);
				ctx.lineTo(o.keypoints[16].x, o.keypoints[16].y);
				ctx.stroke();				


			}



			if (spec.mode == "emoji") {


				var emoji = '😊';

				if (o.keypoints[9].y < o.keypoints[5].y || o.keypoints[10].y < o.keypoints[6].y) {

					emoji = '😡';

				}


				if (Math.abs(o.keypoints[9].y - o.keypoints[10].y) < c.height * 0.15 && Math.abs(o.keypoints[9].x - o.keypoints[10].x) < c.width * 0.15 && Math.abs(o.keypoints[7].y - o.keypoints[9].y) < c.height * 0.15  && Math.abs(o.keypoints[8].y - o.keypoints[10].y) < c.height * 0.15) {

					emoji = '😇';

				}			


				if (Math.abs(o.keypoints[3].y - o.keypoints[9].y) < c.height * 0.15 && Math.abs(o.keypoints[4].y - o.keypoints[10].y) < c.height * 0.15 && Math.abs(o.keypoints[3].x - o.keypoints[9].x) < c.width * 0.15 && Math.abs(o.keypoints[4].x - o.keypoints[10].x) < c.width * 0.15) {

					emoji = '🤯';

				}						

				var size = Math.abs(o.keypoints[3].x - o.keypoints[4].x) * 2;


				ctx.font = size+'px serif';
				ctx.textAlign = "center"; 
				ctx.textBaseline = "middle"; 
				ctx.opacity = 1.0;
				ctx.fillText(emoji, o.keypoints[0].x, o.keypoints[0].y);
	
			} else if (spec.mode == "circle") {

				var size = Math.abs(o.keypoints[9].x - o.keypoints[10].x) / 2;

				ctx.beginPath();

				var cx = o.keypoints[10].x + Math.abs(o.keypoints[9].x - o.keypoints[10].x) / 2;
				var cy = o.keypoints[10].y + (o.keypoints[9].y - o.keypoints[10].y) / 2;


				ctx.arc(cx, cy, size, 0, 2 * Math.PI);
				ctx.strokeStyle = "black";
				ctx.lineWidth = "10";

				var opacity = size / windowWidth;

				ctx.fillStyle =  getColor(opacity);
				ctx.stroke();		
				ctx.fill();						

			}



		});


	})



}





$(document).on("click", "#save", function(){

	var data = {



	};

	data.camera = $("select[name='camera']").val();
	data.overlay = $("select[name='overlay']").val();
	data.color = $("select[name='color']").val();
	data.mode = $("select[name='mode']").val();
	data.tone = $("select[name='tone']").val();

	$.ajax({

		url: "/ar/update.php",
		type: "POST",
		data: {
			id: $("body").attr("q"),
			data: data
		},
		success:function(data){

			window.location.reload();

		}

	})


});