<?

	$q = $_GET["q"];

?>

<!DOCTYPE html>

<html>

	<head>

	    <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>


	    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-uWxY/CJNBR+1zjPWmfnSnVxwRheevXITnMqoEIeG1LJrdI0GlVs/9cVSyPYXdcSF" crossorigin="anonymous">

	    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-kQtW33rZJAHjgefvhyyzcGF3C5TFyBQBA13V1RKPf4uH+bwyzQxZ6CmMZHmNBEfJ" crossorigin="anonymous"></script>

		<!-- Require the peer dependencies of pose-detection. -->
		<script src="https://cdn.jsdelivr.net/npm/@tensorflow/tfjs-core"></script>
		<script src="https://cdn.jsdelivr.net/npm/@tensorflow/tfjs-converter"></script>

		<!-- You must explicitly require a TF.js backend if you're not using the TF.js union bundle. -->
		<script src="https://cdn.jsdelivr.net/npm/@tensorflow/tfjs-backend-webgl"></script>
		<!-- Alternatively you can use the WASM backend: <script src="https://cdn.jsdelivr.net/npm/@tensorflow/tfjs-backend-wasm/dist/tf-backend-wasm.js"></script> -->

		<script src="https://cdn.jsdelivr.net/npm/@tensorflow-models/pose-detection"></script>

	    <!-- Required meta tags -->
	    <meta charset="utf-8">
	    <meta name="viewport" content="width=device-width, initial-scale=1">

		<style>

		@media screen and (-webkit-min-device-pixel-ratio:0) { 
		  select,
		  textarea,
		  input {
		    font-size: 16px;
		  }
		}

		#container
		{
			transform-origin: center;
			opacity: 0.5;
		}

		#container.active
		{
			opacity: 1;
		}

		body[camera='user'] #containerbox
		{
			/*filter: saturate(0);*/
			transform-origin: center;
			transform:scaleX(-1);

		}

		body[tone='bw'] #video
		{
			filter: saturate(0);
		}

		body[tone='sepia'] #video
		{
			filter: sepia(100%);
		}		

		</style>

	</head>

	<body style='margin : 0px; overflow: hidden; position:fixed; left:0; top:0; width:100%; height:100%;' q="<?= $q ?>">

		<script>

			var spec = '<?= file_get_contents(dirname(__FILE__)."/../data/".$q) ?>';
			spec = JSON.parse(spec);

		</script>

		<script src="/ar/self/script.js"></script>

		<div id="" class="position-fixed p-2 bg-white w-100" style="width:100%; z-index:100;">

			<div class="d-flex align-items-center">


				<?

					$model = array(
						"-0" => "---primitives---",
						"geometry:Box" => "Box",
						"geometry:Sphere" => "Sphere",
						"geometry:Cylinder" => "Cylinder",
						"geometry:Text" => "Text",
						"-1" => "---prefab models---",
						"gltf:AntiqueCamera" => "Antique Camera",
						"gltf:Avocado" => "Avocado",
						"gltf:BarramundiFish" => "Barramundi Fish",
						"gltf:BoomBox" => "Boom Box",
						"gltf:Corset" => "Corset",
						"gltf:DamagedHelmet" => "Damaged Helmet",
						"gltf:FlightHelmet" => "Flight Helmet",
						"gltf:Lantern" => "Lantern",
						"gltf:SciFiHelmet" => "Sci Fi Helmet",
						"gltf:Suzanne" => "Suzanne",
						"gltf:WaterBottle" => "Water Bottle",
						"gltf:Duck" => "Duck",
						"gltf:Buggy" => "Buggy",
						"gltf:CesiumMilkTruck" => "Cesium Milk Truck",
						"gltf:RiggedFigure" => "Rigged Figure",
						"gltf:CesiumMan" => "Cesium Man",
						"gltf:BrainStem" => "BrainStem",
						"gltf:Fox" => "Fox",
						"gltf:VC" => "Virtual City",
						"gltf:Sponza" => "Sponza",
						"-2" => "---silhouettes---",
						"silhouette:1" => "Sil. #1",
						"silhouette:2" => "Sil. #2",
						"silhouette:3" => "Sil. #3",
						"silhouette:4" => "Sil. #4",
						"silhouette:5" => "Sil. #5",
						"silhouette:6" => "Sil. #6",
					);



					$rotate = array(
						"" => "None",
						"x" => "Vertical",
						"y" => "Horizontal",
					);



					$scale = array(
						"1" => "Normal",
						"0.5" => "Half",
						"2" => "Double",
						"3" => "Triple",
					);		

					$markers = array(
						"10",
						"41",
						"45",
						"50",
					);	

				?>

				<div class="d-flex align-items-center me-auto">

						<div class="dropdown me-2 markerform" data-marker="eye">
						  <a class="btn btn-outline-dark dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-bs-toggle="dropdown" aria-expanded="false">
						    👁
						  </a>

						  <div class="p-3 dropdown-menu">

							  <h6 class="m-0 mb-1">Overlay</h6>
							  <select name="overlay" class="form-control mb-2">
							  	<option value="dot">dot.body</option>
							  	<option value="skeleton">skeleton (upper)</option>
							  	<option value="skeleton_full">skeleton (full)</option>
							  	<option value="head">dot.head</option>
							  	<option value="wrist_left">dot.wrist (left)</option>
							  	<option value="wrist_right">dot.wrist (right)</option>
							  	<option value="none">none</option>
							  </select>



							  <h6 class="m-0 mb-1">Mode</h6>
							  <select name="mode" class="form-control mb-2">
							  	<option value="">none</option>
							  	<option value="emoji">emoji</option>
							  	<option value="sketch">bodysketch</option>
							  	<option value="circle">circle</option>
							  </select>

						  </div>



						</div>

						<div class="dropdown me-2 markerform" data-marker="eye">
						  <a class="btn btn-outline-dark dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-bs-toggle="dropdown" aria-expanded="false">
						    ⚙️
						  </a>

						  <div class="p-3 dropdown-menu">

							  <h6 class="m-0 mb-1">Camera</h6>
							  <select name="camera" class="form-control mb-2">
							  	<option value="environment">environment</option>
							  	<option value="user">user / mirror</option>
							  </select>


							  <h6 class="m-0 mb-1">Color</h6>
							  <select name="color" class="form-control mb-2">
							  	<option value="basic">basic</option>
							  	<option value="random">random</option>
							  </select>




							  <h6 class="m-0 mb-1">Tone</h6>
							  <select name="tone" class="form-control mb-2">
							  	<option value="color">color</option>
							  	<option value="sepia">sepia</option>
							  	<option value="bw">b&w</option>
							  </select>


						  </div>


						</div>




				</div>


				  <button class="btn btn-dark ms-auto" id="save">
				  	Save
				  </button>
			</div>

		</div>


		<div id="" class="position-fixed border p-0 bg-white w-100 h-100 d-flex flex-column align-center-items justify-content-center" style="width:100%;">

			<div id="containerbox" class="position-relative d-flex mx-auto1 align-center-items justify-content-center">

				<div id="container" class="position-relative">

					<video class="position-relative" style="left:0; top:0;" id="video" playsinline autoplay></video>
					<canvas class="position-absolute" style="left:0; top:0;" id="canvas"></canvas>

				</div>

			</div>




		</div>

		<div id="msg" class="position-fixed text-center p-2 bg-white w-100" style="width:100%; z-index:100; bottom:0;">Loading ...</div>


	</body>


</html>