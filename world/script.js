			//////////////////////////////////////////////////////////////////////////////////
			//		Init
			//////////////////////////////////////////////////////////////////////////////////

			var renderer	= new THREE.WebGLRenderer({
				antialias	: true,
				alpha: true
			});
			renderer.setClearColor(new THREE.Color('lightgrey'), 0)
			renderer.setSize( 1280, 960 );
			renderer.domElement.style.position = 'absolute'
			renderer.domElement.style.top = '0px'
			renderer.domElement.style.left = '0px'
			document.body.appendChild( renderer.domElement );

			// array of functions for the rendering loop
			var onRenderFcts= [];

			// init scene and camera
			var scene	= new THREE.Scene();

			//////////////////////////////////////////////////////////////////////////////////
			//		Initialize a basic camera
			//////////////////////////////////////////////////////////////////////////////////

			// Create a camera
			var camera = new THREE.Camera();
			scene.add(camera);

			////////////////////////////////////////////////////////////////////////////////
			//          handle arToolkitSource
			////////////////////////////////////////////////////////////////////////////////

			var arToolkitSource = new THREEx.ArToolkitSource({
				sourceType : 'webcam',
				sourceWidth: 1280,
				sourceHeight: 960,
				displayWidth: 1280,
				displayHeight: 960,
			})

			arToolkitSource.init(function onReady(){
		        setInterval(() => {
		            onResize()
		        }, 1000);
			})

			// handle resize
			window.addEventListener('resize', function(){
				onResize()
		    })

			function onResize(){
				arToolkitSource.onResizeElement()
				arToolkitSource.copyElementSizeTo(renderer.domElement)
				if( arToolkitContext.arController !== null ){
					arToolkitSource.copyElementSizeTo(arToolkitContext.arController.canvas)
				}
			}
			////////////////////////////////////////////////////////////////////////////////
			//          initialize arToolkitContext
			////////////////////////////////////////////////////////////////////////////////



			// create atToolkitContext
			var arToolkitContext = new THREEx.ArToolkitContext({
				cameraParametersUrl: THREEx.ArToolkitContext.baseURL + '../data/data/camera_para.dat',
				// debug: true,
				detectionMode: 'mono_and_matrix',
			})
			// initialize it
			arToolkitContext.init(function onCompleted(){
				// copy projection matrix to camera
				camera.projectionMatrix.copy( arToolkitContext.getProjectionMatrix() );
			})

			// update artoolkit on every frame
			onRenderFcts.push(function(){
				if( arToolkitSource.ready === false )	return

				arToolkitContext.update( arToolkitSource.domElement )
			})



;(function(){

	var markerRoot1 = new THREE.Group
	markerRoot1.name = 'marker10'
	scene.add(markerRoot1)
	var markerControls = new THREEx.ArMarkerControls(arToolkitContext, markerRoot1, {
		type : 'barcode',
		barcodeValue: 10,
	})

	var markerRoot2 = new THREE.Group
	markerRoot2.name = 'marker41'
	scene.add(markerRoot2)
	var markerControls = new THREEx.ArMarkerControls(arToolkitContext, markerRoot2, {
		type : 'barcode',
		barcodeValue: 41,

	})

	var markerRoot3 = new THREE.Group
	markerRoot3.name = 'marker45'
	scene.add(markerRoot3)
	var markerControls = new THREEx.ArMarkerControls(arToolkitContext, markerRoot3, {
		type : 'barcode',
		barcodeValue: 45,		

	})

	var markerRoot4 = new THREE.Group
	markerRoot4.name = 'marker50'
	scene.add(markerRoot4)
	var markerControls = new THREEx.ArMarkerControls(arToolkitContext, markerRoot4, {
		type : 'barcode',
		barcodeValue: 50,	

	})




})()

window.onload = function () {

	setup();

};



function setup(){



	var markerRoot1 = scene.getObjectByName('marker10')
	var markerRoot2 = scene.getObjectByName('marker41')
	var markerRoot3 = scene.getObjectByName('marker45')
	var markerRoot4 = scene.getObjectByName('marker50')

	var container = new THREE.Group
	scene.add(container)

	var i = 1;

	$.each(spec, function(marker, list){


		$(".markerform[data-marker='"+marker+"'] input[name='text']").val(list.text);
		$(".markerform[data-marker='"+marker+"'] select[name='scale']").val(list.scale);
		$(".markerform[data-marker='"+marker+"'] select[name='model']").val(list.model);
		$(".markerform[data-marker='"+marker+"'] select[name='rotation']").val(list.rotation);

		var markerRoot = scene.getObjectByName('marker'+marker);

		if (list.model == "geometry:Box") {

			var geometry	= new THREE.CubeGeometry(1,1,1);
			var material	= new THREE.MeshNormalMaterial({
				transparent : true,
				opacity: 0.8,
				side: THREE.DoubleSide
			});
			var mesh	= new THREE.Mesh( geometry, material );

			var scale = list.scale;
			mesh.scale.set(scale, scale, scale);
			mesh.position.y	= geometry.parameters.height/2*scale;

			markerRoot.add( mesh );

			if (list.rotation == "x") {


				onRenderFcts.push(function(delta){
					mesh.rotation.x += Math.PI*delta
				})		

			} else if (list.rotation == "y") {


				onRenderFcts.push(function(delta){
					mesh.rotation.y += Math.PI*delta
				})		

			}
	

		} else if (list.model == "geometry:Sphere") {

			var geometry	= new THREE.SphereGeometry(1, 15, 15);
			var material	= new THREE.MeshNormalMaterial({
				transparent : true,
				opacity: 0.8,
				side: THREE.DoubleSide
			});
			var mesh	= new THREE.Mesh( geometry, material );

			var scale = list.scale;
			mesh.scale.set(scale, scale, scale);
			mesh.position.y	= geometry.parameters.height/2*scale;

			markerRoot.add( mesh );


			if (list.rotation == "x") {


				onRenderFcts.push(function(delta){
					mesh.rotation.x += Math.PI*delta
				})		

			} else if (list.rotation == "y") {


				onRenderFcts.push(function(delta){
					mesh.rotation.y += Math.PI*delta
				})		

			}

		} else if (list.model == "geometry:Cylinder") {

			const geometry = new THREE.CylinderGeometry( 1, 1, 1, 32 );
			var material	= new THREE.MeshNormalMaterial({
				transparent : true,
				opacity: 0.8,
				side: THREE.DoubleSide
			});
			var mesh	= new THREE.Mesh( geometry, material );

			var scale = list.scale;
			mesh.scale.set(scale, scale, scale);
			mesh.position.y	= geometry.parameters.height/2*scale;

			markerRoot.add( mesh );

			if (list.rotation == "x") {


				onRenderFcts.push(function(delta){
					mesh.rotation.x += Math.PI*delta
				})		

			} else if (list.rotation == "y") {


				onRenderFcts.push(function(delta){
					mesh.rotation.y += Math.PI*delta
				})		

			}

		} else if (list.model == "geometry:Text") {


			const loader = new THREE.FontLoader();
			const font = loader.load('https://unpkg.com/three@0.77.0/examples/fonts/helvetiker_bold.typeface.json',

				// onLoad callback
				function ( font ) {

					var markerRoot = scene.getObjectByName('marker'+marker);

					var geometry = new THREE.TextGeometry(list.text, {
						font: font,
						size: 0.5,
						height: 0.5,
					} );
					
					var material	= new THREE.MeshNormalMaterial();
					var mesh	= new THREE.Mesh( geometry, material );
					mesh.position.y	= 0.5;

					markerRoot.add( mesh );

					if (list.rotation == "x") {


						onRenderFcts.push(function(delta){
							mesh.rotation.x += Math.PI*delta
						})		

					} else if (list.rotation == "y") {


						onRenderFcts.push(function(delta){
							mesh.rotation.y += Math.PI*delta
						})		

					}

				},

				// onProgress callback
				function ( xhr ) {
					console.log( (xhr.loaded / xhr.total * 100) + '% loaded' );
				},

				// onError callback
				function ( err ) {
					console.log( 'An error happened' );
				}
			);


		} else if (list.model.indexOf("silhouette") > -1) {

			var imageName = list.model.split("silhouette:")[1];



			const loader = new THREE.TextureLoader();

			// load a resource
			loader.load(
				// resource URL
				"/ar/world/silhouette/"+imageName+".png",

				// onLoad callback
				async function ( texture ) {
					// in this example we create the material when the texture is loaded


					var geometry	= new THREE.BoxGeometry(1,1,1);

					var material = new THREE.MeshPhongMaterial( {
						map: texture,
						opacity: 0.8,
						transparent : true,
						side: THREE.FrontSide,
						color:"0xffffff"
						
					 } );

					var mesh	= new THREE.Mesh( geometry, material );

					var scale = list.scale * 10;
					mesh.scale.set(scale, scale, scale*0);
					mesh.position.y	= geometry.parameters.height/2*scale;
					mesh.position.z	= 0;

					markerRoot.add( mesh );




					// var geometry	= new THREE.BoxGeometry(1,1,1);

					// var material	= new THREE.MeshBasicMaterial({
					// 	transparent : true,
					// 	opacity: 0.3,
					// 	side: THREE.DoubleSide,
					// 	color:"0xffffff"
					// });



					// var mesh	= new THREE.Mesh( geometry, material );

					// var scale = list.scale * 5;
					// mesh.scale.set(scale, scale, scale);
					// mesh.position.y	= geometry.parameters.height/2*scale;
					// mesh.position.z	= 0;

					// markerRoot.add( mesh );


					// if (list.rotation == "x") {


					// 	onRenderFcts.push(function(delta){
					// 		mesh.rotation.x += Math.PI*delta
					// 	})		

					// } else if (list.rotation == "y") {


					// 	onRenderFcts.push(function(delta){
					// 		mesh.rotation.y += Math.PI*delta
					// 	})		

					// }
	

				},

				// onProgress callback currently not supported
				undefined,

				// onError callback
				function ( err ) {
					console.error( 'An error happened.' );
				}
			);




		} else {

			var modelname = list.model.split(":")[1];

	        var threeGLTFLoader2 = new THREE.GLTFLoader();
	        threeGLTFLoader2.load("https://raw.githubusercontent.com/KhronosGroup/glTF-Sample-Models/master/2.0/"+modelname+"/glTF/"+modelname+".gltf", function (gltf) {

				var markerRoot = scene.getObjectByName('marker'+marker);

	            model2 = gltf.scene;

				var box = new THREE.Box3().setFromObject( model2 );
				var size = box.getSize();
				var array = [size.x, size.y, size.z];
				var max = Math.max(...array);

				var scale = 3 / max;

	            model2.name = 'Model';

	            scale *= list.scale;

	            model2.scale.set(scale, scale, scale);
				// model2.position.y	= size.y /2*scale;


	            console.log(markerRoot);
				markerRoot.add( model2 );

				if (list.rotation == "x") {


					onRenderFcts.push(function(delta){
						model2.rotation.x += Math.PI*delta
					})		

				} else if (list.rotation == "y") {


					onRenderFcts.push(function(delta){
						model2.rotation.y += Math.PI*delta
					})		

				}				

	        });


		}


		i++;

	})


}


;(function(){




	// onRenderFcts.push(function(){
	// 	if( markerRoot1.visible === true && markerRoot2.visible === true ){
	// 		container.visible = true
	// 	}else{
	// 		container.visible = false
	// 	}
	// })


	// var geometry	= new THREE.CubeGeometry(1,1,1);
	// var material	= new THREE.MeshNormalMaterial({
	// 	transparent : true,
	// 	opacity: 0.8,
	// 	side: THREE.DoubleSide
	// });
	// var mesh	= new THREE.Mesh( geometry, material );
	// mesh.position.y	= geometry.parameters.height/2
	// markerRoot1.add( mesh );



})()

			// as we do changeMatrixMode: 'cameraTransformMatrix', start with invisible scene
			// scene.visible = true

			//////////////////////////////////////////////////////////////////////////////////
			//		render the whole thing on the page
			//////////////////////////////////////////////////////////////////////////////////

	        // var threeGLTFLoader = new THREE.GLTFLoader();
	        // var model;



	        var root = new THREE.Object3D();
	        scene.add(root);



			// var dracoLoader = new THREE.DRACOLoader();
			// dracoLoader.setDecoderPath('./vendor/three.js/build/examples/js/libs/draco');
			// threeGLTFLoader.setDRACOLoader( dracoLoader );

			// const light = new THREE.HemisphereLight( 0xffffbb, 0x080820, 5 );
			const light = new THREE.AmbientLight( 0xffffff, 1 ); // soft white light
			scene.add( light );
			const light2 = new THREE.HemisphereLight( 0xffffbb, 0x080820, 1 );
			scene.add( light2 );

			//////////////////////////////////////////////////////////////////////////////////
			//		render the whole thing on the page
			//////////////////////////////////////////////////////////////////////////////////

			// render the scene
			onRenderFcts.push(function(){
				renderer.render( scene, camera );
			})

			// run the rendering loop
			var lastTimeMsec= null
			requestAnimationFrame(function animate(nowMsec){
				// keep looping
				requestAnimationFrame( animate );
				// measure time
				lastTimeMsec	= lastTimeMsec || nowMsec-1000/60
				var deltaMsec	= Math.min(200, nowMsec - lastTimeMsec)
				lastTimeMsec	= nowMsec
				// call each update function
				onRenderFcts.forEach(function(onRenderFct){
					onRenderFct(deltaMsec/1000, nowMsec/1000)
				})
			})

			// var modelname = "Duck";
	  //       var threeGLTFLoader2 = new THREE.GLTFLoader();

       //  threeGLTFLoader2.load("https://raw.githubusercontent.com/KhronosGroup/glTF-Sample-Models/master/2.0/"+modelname+"/glTF/"+modelname+".gltf", function (gltf) {



	      //       model2 = gltf.scene;

							// var box = new THREE.Box3().setFromObject( model2 );
							// var size = box.getSize();
							// var array = [size.x, size.y, size.z];
							// var max = Math.max(...array);


							// var scale = 3 / max;

	      //       model2.name = 'Model';

							// var markerRoot1 = scene.getObjectByName('marker1');

	      //       // model2.scale.set(0.5, 0.5, 0.5);
	      //       model2.scale.set(scale, scale, scale);


							// // let light = new THREE.DirectionalLight();
							// // light.position.set(0, 5, 0); // Position above the tracking anchor
							// // markerRoot1.add(light);
							// // markerRoot1.add(light.target);

	      //       markerRoot1.add(model2);



       //    });

        // var url = "/demo/arjs/aframe/example s/image-tracking/nft/trex/scene.gltf";

  


    //     threeGLTFLoader.load(url, function (gltf) {
	   //          model = gltf.scene.children[0];
	   //          model.name = 'Model';

	   //          console.log(model);
	   //          // alert(url+"Loaded");

	   //            // model.traverse(function(node){
	   //            //    if (node.isMesh){  
	   //            //      let mat = new THREE.MeshStandardMaterial;
	   //            //      let color = new THREE.Color(0xffffff);
	   //            //      mat.color = color;
	   //            //      mat.wireframe = true;
	   //            //      node.material = mat;                  
	   //            //    }
	   //            // });

				// 		// var markerRoot2 = scene.getObjectByName('marker2')


				// 		// 	var box = new THREE.Box3().setFromObject( model );
				// 		// 	var size = box.getSize();
				// 		// 	var array = [size.x, size.y, size.z];
				// 		// 	var max = Math.max(...array);

				// 		// 	if (max > 3) {

				// 		// 		var scale = 1;

				// 		// 	} else {

				// 		// 		var scale = 3 / max;

				// 		// 	}

	   //   //        model.scale.set(scale, scale, scale);

	   //   //        markerRoot2.add(model);


	   //          // var animation = gltf.animations[0];
	   //          // var mixer = new THREE.AnimationMixer(model);
	   //          // mixers.push(mixer);
	   //          // var action = mixer.clipAction(animation);
	   //          // action.play();

	   //          // root.matrixAutoUpdate = false;
	   //          // root.add(model);


				// // requestAnimationFrame(function animate(nowMsec){
				// // 	// keep looping
				// // 	requestAnimationFrame( animate );
				// // 	// measure time
				// // 	lastTimeMsec	= lastTimeMsec || nowMsec-1000/60
				// // 	var deltaMsec	= Math.min(200, nowMsec - lastTimeMsec)
				// // 	lastTimeMsec	= nowMsec

				// // 	// mesh.rotateY(1);

				// // 	// call each update function
				// // 	onRenderFcts.forEach(function(onRenderFct){
				// // 		onRenderFct(deltaMsec/1000, nowMsec/1000)
				// // 	})
				// // })


	   //      });



$(document).on("click", "#save", function(){

	var data = {



	};

	$(".markerform").each(function(){

		var marker = $(this).attr("data-marker");

		data[marker] = {};
		data[marker].model = $(this).find("select[name='model']").val();
		data[marker].scale = $(this).find("select[name='scale']").val();
		data[marker].rotation = $(this).find("select[name='rotation']").val();
		data[marker].text = $(this).find("input[name='text']").val();

	})

	$.ajax({

		url: "/ar/update.php",
		type: "POST",
		data: {
			id: $("body").attr("q"),
			data: data
		},
		success:function(data){

			window.location.reload();

		}

	})


});