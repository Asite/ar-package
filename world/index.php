<?

	$q = $_GET["q"];

?>

<!DOCTYPE html>

<html>

	<head>
		<!-- three.js library -->
		<script src='/demo/arjs/three.js/examples/vendor/three.js/build/three.min.js'></script>
		<!-- <script src='/demo/arjs/three.js/examples/vendor/three.js/DRACOLoader.js'></script> -->
		<script src='/demo/arjs/three.js/examples/vendor/three.js/GLTFLoader.js'></script>

	    <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>

		<!-- ar.js -->
		<script src="/demo/arjs/three.js/build/ar.js"></script>
		<script>THREEx.ArToolkitContext.baseURL = '/demo/arjs/three.js/examples/../'</script>

	    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-uWxY/CJNBR+1zjPWmfnSnVxwRheevXITnMqoEIeG1LJrdI0GlVs/9cVSyPYXdcSF" crossorigin="anonymous">

	    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-kQtW33rZJAHjgefvhyyzcGF3C5TFyBQBA13V1RKPf4uH+bwyzQxZ6CmMZHmNBEfJ" crossorigin="anonymous"></script>

    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

<style>

@media screen and (-webkit-min-device-pixel-ratio:0) { 
  select,
  textarea,
  input {
    font-size: 16px;
  }
}

</style>

	</head>

	<body style='margin : 0px; overflow: hidden; position:fixed; left:0; top:0; width:100%; height:100%;' q="<?= $q ?>">

		<script>

			var spec = '<?= file_get_contents(dirname(__FILE__)."/../data/".$q) ?>';
			spec = JSON.parse(spec);

		</script>

		<script src="/ar/world/script.js"></script>

		<div id="" class="position-fixed p-2 bg-white w-100" style="width:100%;">

			<div class="d-flex align-items-center">


				<?

					$model = array(
						"-0" => "---primitives---",
						"geometry:Box" => "Box",
						"geometry:Sphere" => "Sphere",
						"geometry:Cylinder" => "Cylinder",
						"geometry:Text" => "Text",
						"-1" => "---prefab models---",
						"gltf:AntiqueCamera" => "Antique Camera",
						"gltf:Avocado" => "Avocado",
						"gltf:BarramundiFish" => "Barramundi Fish",
						"gltf:BoomBox" => "Boom Box",
						"gltf:Corset" => "Corset",
						"gltf:DamagedHelmet" => "Damaged Helmet",
						"gltf:FlightHelmet" => "Flight Helmet",
						"gltf:Lantern" => "Lantern",
						"gltf:SciFiHelmet" => "Sci Fi Helmet",
						"gltf:Suzanne" => "Suzanne",
						"gltf:WaterBottle" => "Water Bottle",
						"gltf:Duck" => "Duck",
						"gltf:Buggy" => "Buggy",
						"gltf:CesiumMilkTruck" => "Cesium Milk Truck",
						"gltf:RiggedFigure" => "Rigged Figure",
						"gltf:CesiumMan" => "Cesium Man",
						"gltf:BrainStem" => "BrainStem",
						"gltf:Fox" => "Fox",
						"gltf:VC" => "Virtual City",
						"gltf:Sponza" => "Sponza",
						"-2" => "---silhouettes---",
						"silhouette:1" => "Sil. #1",
						"silhouette:2" => "Sil. #2",
						"silhouette:3" => "Sil. #3",
						"silhouette:4" => "Sil. #4",
						"silhouette:5" => "Sil. #5",
						"silhouette:6" => "Sil. #6",
					);



					$rotate = array(
						"" => "None",
						"x" => "Vertical",
						"y" => "Horizontal",
					);



					$scale = array(
						"1" => "Normal",
						"0.5" => "Half",
						"2" => "Double",
						"3" => "Triple",
					);		

					$markers = array(
						"10",
						"41",
						"45",
						"50",
					);	

				?>

				<div class="d-flex align-items-center me-auto">

					<? foreach ($markers as $m) { ?>

						<div class="dropdown me-2 markerform" data-marker="<?= $m ?>">
						  <a class="btn btn-outline-dark dropdown-toggle" href="#" role="button" id="dropdownMenuLink" data-bs-toggle="dropdown" aria-expanded="false">
						    <img src="./markers/<?= $m ?>.png" style="height:1.5rem">
						  </a>

						  <div class="p-3 dropdown-menu">

							  <h6 class="m-0 mb-1">Model</h6>
							  <select name="model" class="form-control form-control-lg mb-2">
							  	<? foreach ($model as $k=>$v) { ?>

								  	<option value="<?= $k ?>" <? if (strpos($k, ":") === false) { ?> disabled <? } ?> ><?= $v ?></option>

							  	<? } ?>

							  </select>

							  <h6 class="m-0 mb-1">Rotation</h6>
							  <select name="rotation" class="form-control form-control-lg mb-2">

							  	<? foreach ($rotate as $k=>$v) { ?>

								  	<option value="<?= $k ?>"><?= $v ?></option>

							  	<? } ?>

							  </select>


							  <h6 class="m-0 mb-1">Scale</h6>
							  <select name="scale" class="form-control form-control-lg mb-2">

							  	<? foreach ($scale as $k=>$v) { ?>

								  	<option value="<?= $k ?>"><?= $v ?></option>

							  	<? } ?>

							  </select>						  


							  <h6 class="m-0 mb-1">Text</h6>
							  <input name="text" class="form-control form-control-lg mb-0">



						  </div>


						</div>


					<? } ?>
					<a class="ms-2" target="_blank" href="https://drive.google.com/drive/folders/1Kg68Vs9RFpPbI07wIDTgELjQKUENKRCZ?usp=sharing">Assets</a>

				</div>


				  <button class="btn btn-dark ms-auto" id="save">
				  	Save
				  </button>
			</div>

		</div>

	</body>


</html>