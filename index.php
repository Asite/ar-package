<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=1.0, minimum-scale=1.0, maximum-scale=1.0">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.1.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-uWxY/CJNBR+1zjPWmfnSnVxwRheevXITnMqoEIeG1LJrdI0GlVs/9cVSyPYXdcSF" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-kQtW33rZJAHjgefvhyyzcGF3C5TFyBQBA13V1RKPf4uH+bwyzQxZ6CmMZHmNBEfJ" crossorigin="anonymous"></script>

    <link href="/webfonts/ostrich-sans.css" rel="stylesheet">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Lato:wght@400;700;900&display=swap" rel="stylesheet">


    <link href="/css/style.css" rel="stylesheet">
    <script src="https://code.jquery.com/jquery-3.6.0.min.js" integrity="sha256-/xUj+3OJU5yExlq6GSYGSHk7tPXikynS7ogEvDej/m4=" crossorigin="anonymous"></script>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css">

	<link rel="apple-touch-icon" sizes="180x180" href="/apple-touch-icon.png">
	<link rel="icon" type="image/png" sizes="32x32" href="/favicon-32x32.png">
	<link rel="icon" type="image/png" sizes="16x16" href="/favicon-16x16.png">
	<link rel="manifest" href="/site.webmanifest">

	<!-- Global site tag (gtag.js) - Google Analytics -->
	<script async src="https://www.googletagmanager.com/gtag/js?id=G-3RR6PNF4M7"></script>
	<script>
	  window.dataLayer = window.dataLayer || [];
	  function gtag(){dataLayer.push(arguments);}
	  gtag('js', new Date());

	  gtag('config', 'G-3RR6PNF4M7');
	</script>

	    <script src="/ar/js/script.js"></script>

	    <title>A-SITE BY KRAKLAB</title>
	    <meta property="og:description" content="A-site's cutting-edge, workshops and interactive toolkit series provide artists hands-on opportunities with digital and technological experts." />
	    <meta property="og:image" content="https://workmanarts.com/wp-content/uploads/2021/10/sky-background_1000x1000-1000x600.jpg"/>    
	  </head>
	  <body class="bg-light text-dark m-0 p-0 position-relative w-100 h-100" style="opacity:1">



	  	<div class="container py-5">

	  		<div class="row">

	  			<div class="col-lg-6 offset-lg-3">



			        <div class="mb-3 d-flex align-items-center d-none1">

			          <a class="mx-auto d-flex align-items-center" href="#home" style="pointer-events: all">
			            <svg width="50" height="75" id="logo">
			              <circle style="opacity: 1" r="33.33%" cx="50%" cy="33.33%" fill="#e71d36ff"></circle>
			              <circle style="opacity: 1" r="33.33%" cx="50%" cy="66.66%" fill="#1b998bff"></circle>
			              <ellipse class="st0" cx="50%" cy="50%" rx="15" ry="12.5%" fill="#fff"/>
			            </svg>

			            <div class="mb-0 d-flex align-items-start">

			              <h1 class="m-0 ms-3 fw-black display-3" style="line-height:1; margin-top:0.19em !important">A-SITE</h1>
			              <h5 class="m-0 fw-black ms-2" style="margin-top:0.19em !important">BY KRAKLAB</h5>

			            </div>
			          </a>

			        </div>


			        <div class="mt-5 mx-lg-0 mx-0" >

			        	<a href="/ar/generate.php?mode=self">

			        		<h3 class="border p-3 m-0 border-dark fw-black1 d-flex align-items-center mb-2" ">
			        		
			        			<i class="bi bi-person-fill me-2 d-flex align-items-center flex-column"></i> <span class="d-flex align-items-center flex-column" style="margin-top1:0.19em !important">AR with self</span> <i class="bi bi-chevron-right ms-auto d-flex align-items-center flex-column"></i>

			        		</h3>

			        	</a>

			        	<a href="/ar/generate.php?mode=world">

			        		<h3 class="border p-3 m-0 border-dark fw-black1 d-flex align-items-center">
			        		
			        			<i class="bi bi-globe me-2 d-flex align-items-center flex-column"></i> <span class="d-flex align-items-center flex-column" style="margin-top1:0.19em !important">AR with world</span> <i class="bi bi-chevron-right ms-auto d-flex align-items-center flex-column"></i>

			        		</h3>

			        	</a>


			        </div>

			        <p class='mt-3 text-muted text-center' style="line-height:1.4;">

			        	You must have access to a camera-equipped computing device (laptop or smartphone) to use these experiences. Refer to the following demos to ensure that your device is powerful enough to handle the core technologies.

			        </p>

			        <ul class="list-unstyled text-center">

			        	<li>
			        		<a href="https://storage.googleapis.com/tfjs-models/demos/pose-detection/index.html?model=movenet" target="_blank">Tensorflow JS Demo</a>
			        	</li>
			        	<li>
			        		<a href="https://stemkoski.github.io/AR-Examples/" target="_blank">Three.js and AR.js Examples</a>
			        	</li>

			        </ul>



	  			</div>


	  		</div>


    	</div>



  </body>
</html>